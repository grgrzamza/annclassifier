﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ANNClassifier
{
    class Functions
    {
        public const double THRESH_FUNCTION_ALPHA = 0.1;

        public const int N = 10;

        public static double ThreshFunction(double value)
        {
            return 1 / (1 + Math.Exp(-2 * THRESH_FUNCTION_ALPHA * value));
        }

        public static double EFunction(List<double> result, List<double> expectedResult)
        {
            double value = 0;
            for (int i = 0; i < result.Count; i++)
            {
                value += Math.Pow(result[i] - expectedResult[i], 2);
            }
            return value / 2;
        }
    }
}
