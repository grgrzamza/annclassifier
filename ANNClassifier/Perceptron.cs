﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ANNClassifier
{
    class Perceptron
    {
        public const int DEFAULT_LAYER_COUNT = 1;
        public const int DEFAULT_NEURON_COUNT = 10;
        public const double E_MAX = 0.01;
        public const string FILENAME = "perceptron.txt";

        private int inputCount;
        private int outputCount;
        private int layerCount;
        private int neuronCount;

        public List<List<Node>> Layers;

        public Perceptron(int inputCount, int outputCount, int layerCount = DEFAULT_LAYER_COUNT, int neuronCount = DEFAULT_NEURON_COUNT)
        {
            this.inputCount = inputCount;
            this.outputCount = outputCount;
            this.layerCount = layerCount;
            this.neuronCount = neuronCount;            
        }

        public void InitLayers()
        {
            Layers = new List<List<Node>>();
            for (int i = 0; i < layerCount; i++)
            {
                int nodeCount = i == 0 ? outputCount : neuronCount;
                int nodeInputCount = i == layerCount - 1 ? inputCount : neuronCount;
                Layers.Add(new List<Node>(nodeCount).Select(node => new Node(nodeInputCount, i, new Random().NextDouble() - 0.5)).ToList());
            }
            Layers.Reverse();
        }

        public List<double> Count(List<double> inputs)
        {
            List<double> result = new List<double>();
            foreach (List<Node> layer in Layers)
            {
                result = layer.Select(node => node.CountValue(inputs)).ToList();
                inputs = result;
            }
            return result;
        }

        public bool Train(Tuple<List<double>,List<double>> trainData)
        {
            List<double> result = Count(trainData.Item1);
            double mistake = Functions.EFunction(result, trainData.Item2);
            Console.WriteLine("Expected: {0}, calculated: {1}, mistake: {2}",trainData.Item2, result, mistake);
            if (mistake > E_MAX)
            {
                for (int i = layerCount - 1; i >= 0; i--)
                {
                    List<double> inputs;
                    if (i == 0)
                    {
                        inputs = trainData.Item2;
                    }
                    else
                    {
                        inputs = Layers[i - 1].Select(node => node.lastValue).ToList();
                    }
                    for (int j = 0; j < Layers[i].Count; j++)
                    {
                        if (i == layerCount - 1)
                        {
                            Layers[i][j].FixWeights(inputs, result: trainData.Item2[j]);
                        }
                        else
                        {
                            Layers[i][j].FixWeights(inputs, children: Layers[i + 1]);
                        }
                    }
                }
                return false;
            }
            return true;
        }

        public void Learn(List<Tuple<List<double>, List<double>>> data)
        {
            bool learning = true;
            while (learning)
            {
                learning = false;
                foreach(var trainData in data)
                {
                    if (!Train(trainData) && !learning)
                    {
                        learning = true;
                    }
                }
            }
        }        
    }    
}
