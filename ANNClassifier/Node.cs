﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ANNClassifier
{
    class Node
    {
        private List<double> inputWeights;
        private double offset;
        private int num;
        public double lastValue;
        public double Sigma { get; set; }

        public Node(int inputCount, int num, double offset, List<double> inputWeights = null)
        {
            this.offset = offset;
            this.num = num;
            Random r = new Random();
            this.inputWeights = inputWeights ?? new List<double>(inputCount).Select(value => r.NextDouble() - 0.5).ToList();
        }

        public double CountValue(List<double> inputs)
        {
            double value = 0;
            for (int i = 0; i < inputs.Count; i++)
            {
                value += inputs[i] * inputWeights[i];
            }
            lastValue = Functions.ThreshFunction(value + offset);
            return lastValue;
        }

        public void CountSigma(double? result, List<Node> children)
        {
            Sigma = Functions.THRESH_FUNCTION_ALPHA * lastValue * (1 - lastValue);
            if (children != null)
            {
                double childrenWeights = 0;
                for (int i = 0; i < children.Count; i++)
                {
                    childrenWeights += children[i].Sigma * children[i].inputWeights[num];
                }
                Sigma *= childrenWeights;
            }
            else
            {
                Sigma *= ((double)result - lastValue);
            }
        }

        public void FixWeights(List<double> inputs, double? result = null, List<Node> children = null)
        {
            CountSigma(result, children);
            offset += Functions.THRESH_FUNCTION_ALPHA * Sigma;
            for (int i = 0; i < inputWeights.Count; i++)
            {
                double delta = Functions.THRESH_FUNCTION_ALPHA * Functions.N * inputs[i] * Sigma;
                inputWeights[i] += delta;
            }
        }
    }
}
